# Add WiFi to a Nano using ESP-01 AT commands #

This repository contains the Fritzing project of a board that adds WiFi connectivity to an Arduino Nano using a cheap ESP8266-based ESP-01 board.

The board is specifically designed for the  [atlib library](https://bitbucket.org/augusto_ciuffoletti/atlib) that is extensively documented in my paper presented at [SEUNET](http://www.polymtl.ca/seunet2017/). The library uses the native ESP8266 AT commands, without re-flashing the ESP-01.

![Fritzing](images/schema.png) 

Use the Fritzing schema above to understand board design.

* the power for the ESP-01 comes from the 5V pin of the Arduino Nano, and is stabilized at 3.3V using a 1115 IC. The board can be powered using the USB plug of a PC;
* coupling between CMOS and TTL is by way of a resistive voltage divider (TTL->CMOS) and by direct coupling (CMOS->TTL); 
* the atlib allows a simplified interface with AT commands, leaving enough resources for additional code;
* the communication between the Nano and the ESP-01 use a SoftwareSerial on the Nano that send AT commands to the ESP-01;
* the value of the levelling capacitor (1μF) becomes critical if the input power (from USB) is not sufficient to drive the ESP-01 (100 to 200 mA). In that case it may be the case to significantly increase its value (1000μF and more).

The SCHEMA has been extensively tested, but there is no significant Breadboard and PCB layout, since I used a custom break-out board for the ESP-01. The prototype is shown here

 ![Prototype](images/prototype.jpg)

See the examples section in the  [atlib library](https://bitbucket.org/augusto_ciuffoletti/atlib) to see how to make POST requests to the public ThingSpeak server.

### Status and contact ###

I am now considering the use of a Mini instead of the Nano: no coupling problems, less power, more compact, cheaper. You find the new project in https://bitbucket.org/augusto_ciuffoletti/wifimini.

Contact me at: augusto@di.unipi.it or augusto.ciuffoletti@gmail.com.